# ifndef HYDRO_DRIVER_H
# define HYDRO_DRIVER_H

#include "hydro_data.hpp"

struct HydroDriver{

    /**
     * @brief Construct a new hydro driver object
     * 
     * @param initial_hydro_data The initial hydro_data state
     * @param max_initial_dt The maximal initial dt allowed
     */
    HydroDriver(Geometry const geometry,
                double const gamma,
                std::vector<double> const init_position,
                std::vector<double> const init_velocity,
                std::vector<double> const init_pressure,
                std::vector<double> const init_rho,
                double const artificial_viscosity_sigma,
                double const max_initial_dt, 
                double const dt_factor,
                double const dt_density_epsilon,
                double const dt_courant_factor);

    /**
     * @brief Calculates the pressure in the cells using the ideal gas EOS
     * 
     */
    void solve_for_pressure();

    /**
     * @brief Advances the HydroData object one dt step in time 
     * 
     * @return double The step dt
     */
    double step();
    
    /**
     * @brief Calculates the maximal allowed dt due to numerical stability using Courant's condition
     * 
     * @return double The dt derived from Courant's condition
     */
    double dt_courant();

    /**
     * @brief Calculates the maximal allowed dt, allowing the density to vary by at most a factor of eps
     * 
     * @param eps The maximal allowed relative change of the density
     * @return double The dt derived from the above limitation
     */
    double dt_density();

    Geometry const geometry;
    double const gamma;
    HydroData data;
    HydroData old_data;
    double dt;
    double const artificial_viscosity_sigma;
    double const dt_factor;
    double const dt_density_epsilon;
    double const dt_courant_factor;

};

# endif // HYDRO_DRIVER_H
