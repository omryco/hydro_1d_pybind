# ifndef HYDRO_DATA_H
# define HYDRO_DATA_H


# include <vector>
# include "geometry.hpp"


struct HydroData {

    // methods for ensuring data self-consistency
    void calculate_volumes(Geometry const &geometry);
    void calculate_artificial_viscosity(double const sigma);
    void calculate_acceleration(Geometry const &geometry);

    // constructor
    HydroData(Geometry const &geometry,
              std::vector<double> const position,
              std::vector<double> const velocity,
              std::vector<double> const pressure,
              std::vector<double> const rho);

    // vertices attributes
    std::vector<double> position;
    std::vector<double> velocity; 
    std::vector<double> acceleration;    
    std::vector<double> vertex_mass;    

    // cells attributs
    std::vector<double> pressure; 
    std::vector<double> mass;
    std::vector<double> rho;
    std::vector<double> volume;
    std::vector<double> artificial_viscosity;

};

# endif // HYDRO_DATA_H
