# include "hydro_driver.hpp"
# include <algorithm>
# include <utility>
# include <cmath>


HydroDriver::HydroDriver(Geometry const geometry,
                         double const gamma,
                         std::vector<double> const init_position,
                         std::vector<double> const init_velocity,
                         std::vector<double> const init_pressure,
                         std::vector<double> const init_rho,
                         double const artificial_viscosity_sigma,
                         double const max_initial_dt, 
                         double const dt_factor,
                         double const dt_density_epsilon,
                         double const dt_courant_factor): 
                         data(HydroData(geometry, 
                                        init_position,
                                        init_velocity,
                                        init_pressure,
                                        init_rho)), 
                         old_data(HydroData(geometry,
                                            init_position,
                                            init_velocity,
                                            init_pressure,
                                            init_rho)), 
                         geometry(geometry), gamma(gamma), dt(max_initial_dt/dt_factor), 
                         dt_factor(dt_factor), artificial_viscosity_sigma(artificial_viscosity_sigma),
                         dt_density_epsilon(dt_density_epsilon), dt_courant_factor(dt_courant_factor) {
                             data.calculate_artificial_viscosity(artificial_viscosity_sigma);
                         }


double HydroDriver::step() {

    // calculating this step's dt
    dt = std::min(dt * dt_factor, std::min(dt_courant(), dt_density()));

    // swapping the new and old HydroData members
    //std::swap(old_data, data);
    old_data = data; 

    // stepping the partitions' positions with half step velocity      
    for (unsigned int i=0; i < data.velocity.size(); ++i){
        data.velocity[i] += data.acceleration[i] * dt / 2.;
        data.position[i] += data.velocity[i] * dt;
    }

    // calculating the new volumes and artificial viscosities
    data.calculate_volumes(geometry);
    data.calculate_artificial_viscosity(artificial_viscosity_sigma);

    //calculating new pressures
    solve_for_pressure();

    // stepping the partitions' velocity a half step using the new accelerations
    data.calculate_acceleration(geometry);
    for (unsigned int i=0; i < data.velocity.size(); ++i){
        data.velocity[i] += data.acceleration[i] * dt / 2.;
    }

    return dt;
}


void HydroDriver::solve_for_pressure() {
    
    // this should generally be done using a general EOS
    for (unsigned int i=0; i < data.pressure.size(); ++i) {
        double const aux = (data.volume[i] - old_data.volume[i]) / 2;
        data.pressure[i] = 
            (old_data.volume[i] / (gamma - 1) * old_data.pressure[i] - aux *
            (old_data.pressure[i] + data.artificial_viscosity[i] + old_data.artificial_viscosity[i])) / 
            (data.volume[i] / (gamma - 1) + aux);
    }
}


double HydroDriver::dt_courant(){

    double inv = 0.;
    for (unsigned int i=0; i < data.pressure.size(); ++i){
        double const aux = (gamma * data.pressure[i] * data.volume[i] / data.mass[i] + 
              std::abs(data.velocity[i+1]) + std::abs(data.velocity[i])) / 
              (data.position[i+1] - data.position[i]);
        if (aux > inv) inv = aux;
    }   
    return dt_courant_factor / inv;
}


double HydroDriver::dt_density(){

    double inv = 0.;
    for (unsigned int i=0; i < data.pressure.size(); ++i){
        double const aux = std::abs((data.volume[i] - old_data.volume[i]) / 
                       (data.volume[i] + old_data.volume[i]));
        if (aux > inv) inv = aux;
    }   
    return 0.5 * dt * dt_density_epsilon / inv;
}
