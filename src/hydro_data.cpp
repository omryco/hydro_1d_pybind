# include <vector>
# include <algorithm>
# include <cassert>
# include "geometry.hpp"
# include "hydro_data.hpp"


HydroData::HydroData(Geometry const &geometry, 
                     std::vector<double> const position,
                     std::vector<double> const velocity,
                     std::vector<double> const pressure,
                     std::vector<double> const rho): 
                     position(position),
                     velocity(velocity),
                     acceleration(pressure.size()),
                     rho(rho),
                     pressure(pressure),
                     mass(pressure.size()),
                     vertex_mass(pressure.size() + 1),
                     volume(pressure.size()),
                     artificial_viscosity(pressure.size()) {
        
        assert(position.size() == velocity.size());
        assert(velocity.size() == pressure.size() + 1);
        assert(pressure.size() == rho.size());

        // calculate cells volumes
        calculate_volumes(geometry);

        // calculate cells and partitions mass
        mass[0] = volume[0] * rho[0];
        vertex_mass[0] = 0.5 * mass[0];
        for (unsigned int i=1; i < mass.size(); ++i){
            mass[i] = volume[i] * rho[i];
            vertex_mass[i] = 0.5 * (mass[i] + mass[i-1]);
        }
        vertex_mass.back() = 0.5 * mass.back();

        // Applying rigid boundary conditions implicitly
        acceleration.front() = 0.;
        acceleration.back() = 0.;
        calculate_acceleration(geometry);
    }


void HydroData::calculate_volumes(Geometry const &geometry){
    double aux1 = geometry.beta / (geometry.alpha + 1) * std::pow(position[0], geometry.alpha + 1);

    for (unsigned int i=0; i < volume.size(); ++i){
        double const aux2 = geometry.beta / (geometry.alpha + 1) * std::pow(position[i+1], geometry.alpha + 1);
        volume[i] = aux2 - aux1;
        rho[i] = mass[i] / volume[i];
        aux1 = aux2;
    }
}


void HydroData::calculate_artificial_viscosity(double const sigma){
    for (unsigned int i=0; i < artificial_viscosity.size(); ++i){
        double const diff = velocity[i+1] - velocity[i];
        if (diff < 0){
            artificial_viscosity[i] = sigma * diff * diff * mass[i] / volume[i];
        }
        else {
            artificial_viscosity[i] = 0;
        }
    }
}


void HydroData::calculate_acceleration(Geometry const &geometry){
    double aux1 = pressure[0] + artificial_viscosity[0];
    for (unsigned int i=1; i < pressure.size(); ++i){
        double const aux2 = pressure[i] + artificial_viscosity[i];
        acceleration[i] = - geometry.beta * std::pow(position[i], geometry.alpha) * (aux2 - aux1) / vertex_mass[i];
        aux1 = aux2;
    }
}
