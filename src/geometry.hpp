# ifndef GEOMETRY_H
# define GEOMETRY_H

# define _USE_MATH_DEFINES
# include <cmath>
# include <vector>

struct Geometry
{

    Geometry(double const alpha, double const beta);
    
    double const alpha;
    double const beta;
    
};

namespace geometries {
    Geometry const PLANAR(0, 1);
    Geometry const SPHERICAL(2, 4 * M_PI);
    Geometry const CYLINDRICAL(1, 2 * M_PI); 
}

# endif
